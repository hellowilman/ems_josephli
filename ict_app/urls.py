from django.conf.urls import patterns, include, url
from django.contrib import admin
from EMS import urls as EMS_URLS

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ict_app.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),


    url(r'^admin/', include(admin.site.urls)),
    url(r'^ems/',include(EMS_URLS)),
    url(r'^$', EMS_URLS.html_view.index, name='home'),
)
