from django.shortcuts import render
from django.http import HttpResponse 
from utils.webutils import http_ok , rget2 as get, http_fail as fail,is_auth,send_email
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,logout,login
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime
from models import Booking, Equipment, LogData,QnATicket
from django.contrib.auth import get_user
import simplejson as json

# Create your views here.
def jason(r):
   
    return HttpResponse("jason is very handsome!")


def test(request):
    return HttpResponse("hello world. You are at the user test page.")

app_id = "test_app"


def signup(request):
    email = get(request,"email")
    uname = get(request,"uname")
    passwd = get(request,"passwd")
    app = get(request,"app")
    # check the input
    if not email:
         return fail("No email") 
      # also need to check the email here
    if not passwd:
         return fail("No password")
    if not uname:
         uname = email
    if app:
        app_id = app
    else:
        app_id = "test_app"
    uAppName = "test_app" + ","+email
    quser = User.objects.filter(username = uAppName)
    if quser:
       return fail("User Exist")
    user = User.objects.create_user(uAppName,email,passwd)
    user.first_name = uname
    user.save()
 
    # check the username 

    return http_ok({"userid":user.id,"name":uname,"uname":user.username}) 


def query(request):
    if not is_auth(request):
        return fail("not login yet")
    user = request.user
    return http_ok({"userid":user.id,
          "email":user.email,"uname":user.username,"last_name":user.last_name,
          "first_name":user.first_name}) 
   

def delete(request):
    if not is_auth(request):
       return fail("not login yet")
    uname = get(request,"email")
      
    return HttpResponse("delete")

def update(request):
    if not is_auth(request):
       return fail("not login yet")
    user = request.user
    fname = get(request,"first_name")
    if fname:
       user.first_name = fname
    lname = get(request,"last_name")      
    if lname:
       user.last_name = lname
    user.save()
    return http_ok()

@csrf_exempt
def login_view(request):
    username = get(request,"username")
    passwd = get(request,"passwd")
    # check input
    if not username or not passwd:
       return fail("Arg error. Param {email,passwd}")
    user = authenticate(username=username,password = passwd)
    if not user:
       return fail("User and password error!")
    if not user.is_active:
       return fail("Disabled")
    login(request,user)
    return http_ok({"userid":user.id,"name":user.first_name,"uname":user.username})

def signin(request):
    email = get(request,"email")
    passwd = get(request,"passwd")
    app = get(request,"app")

    # check input 
    if not email or not passwd:
       return fail("Arg error. Param {email,passwd}")
    # do auth
    if app:
        app_id = app
    else:
        app_id = "test_app"

    uAppName =  app_id + "," + email
    user = authenticate(username=uAppName,password = passwd) 
    if not user:
       return fail("User and password error!")
    if not user.is_active:
       return fail("Disabled")
    login(request,user) 
    return http_ok({"userid":user.id,"name":user.first_name,"uname":user.username}) 



def signout(request):
    if not request.user.is_authenticated():
        return fail("not login")
    logout(request)
    return http_ok() 

def get_equip_list(request):
    datestr = get(request,"date")
    if datestr:
        date = datetime.strptime(datestr,'%m/%d/%Y') # 09/18/2014
        bks = Booking().filter_date(date)
        print 'bks', len(bks)
        bks_ID = []
        if bks :
            for bk in bks:
                bks_ID.append(bk.equip.id)
        print bks_ID
        eqs1 = Equipment.objects.exclude(id__in = bks_ID)
        eqs2 = Equipment.objects.filter(id__in = bks_ID)

        jdata = []
        if eqs1:
            for eq in eqs1:
                jdata.append({'id':eq.id, 'name':eq.name,'status':eq.status.name,'booked':0,'location':eq.location.name})
        if eqs2:
            for eq in eqs2:
                jdata.append({'id':eq.id, 'name':eq.name,'status':eq.status.name,'booked':1,'location':eq.location.name})

        return http_ok(jdata)
    else:
        return fail("No date input. e.g. ?date=09/18/2014")

@csrf_exempt
@login_required
def book_equipment(req):

    eq_id = get(req,"eq_id")
    datestr = get(req,"date")

    if eq_id is not None and datestr is not None:
        eq = Equipment.objects.filter(id = eq_id)
        user = get_user(req)
        book_date = datetime.strptime(datestr,'%m/%d/%Y')

        book = Booking().book(equip=eq[0], book_date=book_date,user=user)
        if book:
            return http_ok({'id':book.id})
        else:
            return fail("Booking failed. Please try again later!")
    else:
        return fail("Input error! Please post the data in format as {eq_id:1,date:09/18/2014}")


@csrf_exempt
@login_required
def book_delete(req):
    b_id = get(req,"bid")
    if b_id is not None:
        b = Booking.objects.filter(id = b_id)
        if b:
            b.delete()
            return http_ok()

    return fail("Cannot find the equipment")

@csrf_exempt
def qaticket(req):
    name = get(req,"name")
    email = get(req,"email")
    msg = get(req,"msg")
    print "add data",name,email,msg
    if name and email and msg:
        qa  = QnATicket().add(name,email,msg)
        return http_ok({"id":qa.id})
    else:
        return fail("Input error!")
















 
