from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth import get_user
from datetime import datetime
from utils.webutils import send_email
# Create your models here.

#class UserInfo(models.Model):
#     id = models.ForeignKey(User)
#     img_url = models.CharField(max_length=1024)
class STATUS(models.Model):
    name = models.CharField(max_length=32)
    def __unicode__(self):
        return self.name
      # ON_HOLD = 1
    # AVAILABLE = 2
    # IN_REPAIR = 3
    # CHECK_OUT = 4




class ACTIONS:
    CHECK_OUT = 1
    CO = 1
    RETURN = 2
    REPAIR = 3
    ADD = 4
    DELETE = 5
    UPDATE = 6
    BOOK_EQ = 7

class Location(models.Model):
    name = models.CharField(max_length=32)
    def __unicode__(self):
        return self.name


class Equipment(models.Model):
    name = models.CharField(max_length=1024)
    type = models.CharField(max_length=1024)
    location = models.ForeignKey(Location)
    date_purchase = models.DateTimeField()
    remark = models.CharField(max_length=1024)
    status = models.ForeignKey(STATUS)
    user = models.ForeignKey(User,blank=True,null=True) # the person who check out / occupy this equipment
    privilege_level = models.IntegerField(default = 0)

    def __unicode__(self):
        return self.name


    def add_equipment(self, name,type, location, date_purchase, remark):
        eq = Equipment(name = name, type = type, location = location, date_purchase =date_purchase, remark = remark)
        if eq:
            eq.save()
            return eq
        else:
            return None

    def check_out(self, equip_id, user):
        eq = Equipment.objects.filter(key = equip_id)
        if eq:
            eq[0].status = STATUS.CHECK_OUT
            eq[0].user = user
            eq[0].save()
            return LogData.log(user, ACTIONS.CO)

        else:
            return None

    def return_equipment(self,equip_id,user):
        eq = Equipment.objects.filter(key = equip_id)
        if eq:
            eq[0].status = STATUS.AVAILABLE
            eq[0].user = None
            eq[0].save()
            return LogData.log(user, self.ACTIONS.RETURN)
        else:
            return None
    def search(self,key,flag = 'name'):
        if flag == 'type':
            eq = Equipment.objects.filter(type__contains  = key)

        if flag == 'name':
            eq = Equipment.objects.filter(name__contains  = key)

        if flag =='location':
            eq = Equipment.objects.filter(location__contains  = key)

        if flag =='status':
            eq = Equipment.objects.filter(status = key)
        return eq

    def delete(self,equip_id,user):
        eq = Equipment.objects.filter(key = equip_id)
        if eq:
            eq.delete()
            return LogData.log(user, ACTIONS.DELETE)
        else:
            return 0
    def get_all(self):
        eq = Equipment.objects()
        return eq

class Booking(models.Model):
    own = models.ForeignKey(User)
    date_create = models.DateTimeField()
    equip = models.ForeignKey(Equipment)
    book_date = models.DateField()
    status = models.ForeignKey(STATUS)
    def __unicode__(self):
        return self.own.username   +'/'+ self.equip.name+'/ ' + self.book_date.strftime("%Y-%m-%d")
    def check_avaible(self,equip,date):
        bookings = Booking.objects.filter(equip = equip,book_date = date)
        if not bookings:
            return True
        else:
            return False

    def filter_date(self,date):
        bookings = Booking.objects.filter(book_date = date)
        return bookings

    def get_user_booking(self,user):
        bookings = Booking.objects.filter(own = user)
        return bookings

    def book(self,equip,book_date,user):
        eq = Booking.objects.filter(equip = equip,book_date = book_date)
        if not eq:
            d = datetime.now()
            b = Booking(own = user, equip = equip, book_date = book_date,date_create = d, status_id =3)
            if b:
                b.save()
                #b.status_id = 3 # update booking status
                equip.save()
                LogData().log(user, ACTIONS.BOOK_EQ)
                return b
        else:
            return None

    def cancel(self,book_id,user):
        b = Booking.objects.filter(key = book_id,own = user)
        if b:
            b.delete()
        else:
            return None



class LogData(models.Model):
    user = models.ForeignKey(User)
    date = models.DateTimeField()
    action = models.IntegerField()
    remark = models.CharField(max_length=1024)

    def log(self,user,action,remark="N/A"):
        print 'log',user
        d = datetime.now()
        newlog = LogData(user=user, date=d, action=action, remark=remark)
        if newlog:
            newlog.save()
            return True
        else:
            return None


class Privilege(models.Model):
    user = models.ForeignKey(User)
    privilege_level = models.IntegerField()

    class LEVEL:
        STUDENT =1
        TEACHER = 2
        ADMIN = 3

    def add(self,user,level):
        Privilege.objects.get_or_create()


class QnATicket(models.Model):
    user_from = models.CharField(max_length=1024)
    email = models.CharField(max_length=1024)
    msg = models.CharField(max_length= 2048)
    resp = models.CharField(max_length = 128,blank=True)


    def add(self,user_name,email,msg):
        email_msg = 'Dear %s,\n  We have received your message and will replay to you soon! Thank you!n \n Message: %s' % (user_name,msg)
        qa = QnATicket(user_from = user_name,email = email, msg = msg)
        if qa:
            qa.save()
            try:
                send_email([email,'lizexiajoseph@gmail.com'],'lizexiajoseph@gmail.com',email_msg)
                #send_email('lizexiajoseph@gmail.com',email,'%s sent you an message!\n %s\n%s',(user_name,email,msg))
            except:
                print 'error: failed to send email'
            return qa
        else:
            return None
    def __unicode__(self):
        return self.user_from+"(" + self.email+"):"+self.msg

    def resp(self,ticket_id,remark):
        qa = QnATicket.objects.filter(key = ticket_id)
        if qa:
            qa.resp = remark
            qa.save()
            return True
        else:
            return None







