__author__ = 'LiJoseph'
from models import Equipment, Booking
from django.contrib.auth import get_user
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.http.response import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import simplejson as json
from datetime import datetime

def render_with_user(template,req,data={}):
    user = get_user(req)
    if( not user.is_anonymous()) and user.is_authenticated():
        data["user"] = user
        print user
    return render_to_response(template,data)

@login_required
def booking_date_view(req):
    return render_with_user("new_booking.html",req)

def login_view(req):
    return render_with_user("login.html",req)

@csrf_exempt
@login_required
def booking_view(req):
    data = []
    if req.method == 'POST':
        eq_id = req.POST.get("eq_id",None)
        if eq_id is not None:
            eq = Equipment.objects.filter(id = eq_id)
            user = get_user(req)
            d = datetime.now()

            book = Booking().book(equip=eq[0], book_date=d,user=user)
            if book:
                datastr = json.dumps({'id':book.id})
            else:
                datastr = ''
            return HttpResponse(datastr)

    else:
        eq = Equipment.objects.all()
        if eq:
            for e in eq:
                data.append({'id':e.id,'name':e.name, 'status':e.status.name, 'date':e.date_purchase.year,'location':e.location.name})

    resp = render_to_response("booking.html",{'equipments':data})
    return resp


def index(req):
    return render_with_user("index.html",req)

@csrf_exempt
@login_required
def mybooking(req):
    user = get_user(req)
    data = {}
    if user.is_authenticated():
        bookings = Booking().get_user_booking(user)
        bdata =[]
        for b in bookings:
            bdata.append({"id":b.id,"equip":b.equip.name,"equip_id":b.equip.id,"date":b.book_date.strftime("%Y%m%d"),
                          "cdata":b.date_create.strftime("%Y%m%d %H:%M")})
        data["bdata"] = bdata
    print data
    return render_with_user("mybooking.html",req,data)

def support(req):

    return render_with_user("support.html",req)
