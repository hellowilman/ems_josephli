from django.conf.urls import patterns, include, url

from EMS import views,html_view

urlpatterns = patterns('user',
    # Examples:
    # url(r'^$', 'gApp.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^api/signup/',views.signup,name='signup'),
    url(r'^api/login/',views.login_view,name='login'),
    url(r'^api/signin/',views.signin,name='signin'),
    url(r'^api/query/',views.query,name='query'),
    url(r'^api/delete/',views.delete,name='delete'),
    url(r'^api/update/',views.update,name='update'),
    url(r'^api/signout/',views.signout,name='signout'),
    url(r'^api/list/equip/bydate/',views.get_equip_list,name='eqlist'),
    url(r'^api/book/equip/',views.book_equipment,name='book_equip'),
    url(r'^api/book/delete/',views.book_delete,name='book_delete'),
    url(r'^api/ticket/add/',views.qaticket,name='book_delete'),


    url(r'^web/booking/',html_view.booking_view,name="booking"),
    url(r'^web/booking_date/',html_view.booking_date_view,name="booking"),
    url(r'^web/login/',html_view.login_view,name="booking"),
    url(r'^web/index/',html_view.index,name="index"),
    url(r'^web/my_booking/',html_view.mybooking,name="mybooking"),
    url(r'^web/support/',html_view.support,name="support")

)
