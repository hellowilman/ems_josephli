from django.contrib import admin
from models import Equipment,Location,STATUS,Booking,QnATicket


# Register your models here.
@admin.register(Equipment)
class EquipmentAdmin(admin.ModelAdmin):
    pass

@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    pass

@admin.register(STATUS)
class StatusAdmin(admin.ModelAdmin):
    pass


@admin.register(Booking)
class BookingAdmin(admin.ModelAdmin):
    pass

@admin.register(QnATicket)
class QnAAdmin(admin.ModelAdmin):
    pass

